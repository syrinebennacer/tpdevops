# Programme faire une calculatrice simple

# Cette fonction additionne deux nombres
def add(x, y):
    return x + y

# Cette fonction soustrait deux nombres
def subtract(x, y):
    return x - y

# Cette fonction multiplie deux nombres
def multiply(x, y):
    return x * y

# Cette fonction divise deux nombres
def divide(x, y):
    return x / y


print("Select operation.")
print("1.Add")
print("2.Subtract")
print("3.Multiply")
print("4.Divide")

while True:
    # prendre l'entrée de l'utilisateur
    choice = input("Enter choice(1/2/3/4): ")

    # vérifier si le choix est l'une des quatre options
    if choice in ('1', '2', '3', '4'):
        num1 = float(input("Enter first number: "))
        num2 = float(input("Enter second number: "))

        if choice == '1':
            print(num1, "+", num2, "=", add(num1, num2))

        elif choice == '2':
            print(num1, "-", num2, "=", subtract(num1, num2))

        elif choice == '3':
            print(num1, "*", num2, "=", multiply(num1, num2))

        elif choice == '4':
            print(num1, "/", num2, "=", divide(num1, num2))
        
        # vérifier si l'utilisateur veut un autre calcul
        # rompre la boucle while si la réponse est non
        
        next_calculation = input("Let's do next calculation? (yes/no): ")
        if next_calculation == "no":
          break
    
    else:
        print("Invalid Input")
