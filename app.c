#include <stdio.h>  // Inclure la bibliothèque STanDard Input Output

                    // permet d'utiliser, entre autre, printf et scanf
int main() 
{
	int nb;	// Entier : nb
	
	printf("Pair ou impair ?\n");     // Afficher : "Pair ou impair ?"
	
	printf("Entrez un nombre entier : "); // Afficher : "Entrez un nombre entier : "
	scanf("%d", &nb);			// Entrer : nb

	if(nb % 2 == 0) {           // Si(nb % 2 = 0)
    	printf("\n%d est pair\n", nb);   // Afficher : "nb est pair"
	}  
	else {                      // Sinon
	    printf("\n%d est impair\n", nb); // Afficher : "nb est impair"
	}
		
	return 0;
}
